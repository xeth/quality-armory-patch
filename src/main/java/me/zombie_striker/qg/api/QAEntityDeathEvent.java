/**
 * Custom event called when entity gets killed by gun
 */

package me.zombie_striker.qg.api;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.zombie_striker.qg.guns.Gun;

public class QAEntityDeathEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();

	private boolean cancel = false;
	private Entity entity;
	private Entity killer;
	private Gun gun;

	public QAEntityDeathEvent(Entity entity, Entity killer, Gun gun) {
		this.entity = entity;
		this.killer = killer;
		this.gun = gun;
	}

	public Gun getGun() {
		return gun;
	}

	public Entity getEntity() {
		return entity;
	}
	public Entity getKiller() {
		return killer;
	}

	public boolean isCancelled() {
		return cancel;
	}

	public void setCancelled(boolean cancelled) {
		this.cancel = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}