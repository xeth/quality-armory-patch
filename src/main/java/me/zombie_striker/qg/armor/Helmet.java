package me.zombie_striker.qg.armor;

import java.util.List;
import java.util.UUID;
import java.util.Map;
import java.util.HashMap;

import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;

import me.zombie_striker.customitemmanager.CustomBaseObject;
import me.zombie_striker.customitemmanager.CustomItemManager;
import me.zombie_striker.customitemmanager.MaterialStorage;

public class Helmet extends ArmorObject {
	int armor;

	// enchants
	final HashMap<Enchantment, Integer> enchants;

	public Helmet(String name, String displayname, List<String> lore, ItemStack[] ing, MaterialStorage st,
			double cost, int armor, HashMap<Enchantment, Integer> enchants) {
		super(name, displayname, lore, ing, st, cost);
		this.armor = armor;
		this.enchants = enchants;
	}

	@Override
	public ItemStack getItemStack() {
		ItemStack is = CustomItemManager.getItemType("gun").getItem(this.getItemData().getMat(), this.getItemData().getData(), this.getItemData().getVariant());
		ItemMeta im = is.getItemMeta();

		// set armor
		AttributeModifier modifier = new AttributeModifier(
			UUID.randomUUID(),
			"armor_helmet",
			(double) this.armor,
			AttributeModifier.Operation.ADD_NUMBER,
			EquipmentSlot.HEAD
		);
		im.addAttributeModifier(Attribute.GENERIC_ARMOR, modifier);
		im.removeAttributeModifier(Attribute.GENERIC_ATTACK_SPEED);
		im.removeItemFlags(ItemFlag.HIDE_ATTRIBUTES);

		is.setItemMeta(im);

		// apply enchantments
		is.addUnsafeEnchantments(this.enchants);

		return is;
	}
}
