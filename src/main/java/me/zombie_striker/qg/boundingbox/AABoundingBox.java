package me.zombie_striker.qg.boundingbox;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Entity;

/**
 * Axis aligned bounding box
 */
public class AABoundingBox implements AbstractBoundingBox {
    public double xmin;
    public double xmax;
	public double ymin;
    public double ymax;
    public double zmin;
    public double zmax;

    public AABoundingBox(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
        this.xmin = xmin;
        this.xmax = xmax;
        this.ymin = ymin;
        this.ymax = ymax;
        this.zmin = zmin;
        this.zmax = zmax;
    }
    
    public void update(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
        this.xmin = xmin;
        this.xmax = xmax;
        this.ymin = ymin;
        this.ymax = ymax;
        this.zmin = zmin;
        this.zmax = zmax;
    }
	
	@Override
	public boolean intersects(Entity shooter, Location check, Entity base) {
		double x = check.getX();
		double y = check.getY();
		double z = check.getZ();

		return (x >= xmin && x <= xmax) && (y >= ymin && y <= ymax) && (z >= zmin && z <= zmax);
	}

	@Override
	public boolean allowsHeadshots() {
		return false;
	}

	@Override
	public boolean intersectsBody(Location check, Entity base) {
        return false;
	}

	@Override
	public boolean intersectsHead(Location check, Entity base) {
        return false;
	}


	@Override
	public double maximumCheckingDistance(Entity base) {
        return 1.0;
	}

	/**
	 * Perform intersect tests in a volume centered around region
	 * and visualize hit locations with particles
	 */
	public void debug(Location loc) {
		final double STEP = 0.5;

		World world = loc.getWorld();
		Location test = loc.clone();

		for ( double y = this.ymin; y < this.ymax; y += STEP ) {
			for ( double x = this.xmin; x < this.xmax; x += STEP ) {
				for ( double z = this.zmin; z < this.zmax; z += STEP ) {
					test.setX(x);
					test.setY(y);
					test.setZ(z);

					if ( this.intersects(null, test, null) ) {
						world.spawnParticle(
							Particle.VILLAGER_HAPPY,
							x,
							y,
							z,
							1
						);
					}
				}
			}
		}
	}
}
