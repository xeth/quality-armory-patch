/**
 * Modified player bounding box tests:
 * - try to make easier to hit
 * - no headshots (simplify hit logic)
 */

package me.zombie_striker.qg.boundingbox;

import me.zombie_striker.qg.handlers.BoundingBoxUtil;
import org.bukkit.Location;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class PlayerBoundingBox2 implements AbstractBoundingBox{

	private double bodyWidthRadius = 0.55;

	private double bodyHeight = 1.95;
	private double headTopHeight = 1.95;

	private double headTopCrouching = 1.49;
	private double bodyHeightCrouching = 1.49;

	public PlayerBoundingBox2() {
		//BoundingBoxManager.addBoundingBox("defaulthumanoid", this);
	}
	public PlayerBoundingBox2(double bodyHeight, double bodyRadius, double headTopHeight) {
		this();
		this.bodyHeight = bodyHeight;
		this.bodyWidthRadius = bodyRadius;
		this.headTopHeight = headTopHeight;

	}
	
	@Override
	public boolean intersects(Entity shooter, Location check, Entity base) {
		return intersectsBody(check, base);
	}

	@Override
	public boolean allowsHeadshots() {
		return true;
	}

	@Override
	public boolean intersectsHead(Location check, Entity base) {
		// if(((Player)base).isSneaking()){
		// 	return BoundingBoxUtil.within2DHeight(base,check,(headTopCrouching-bodyHeightCrouching), bodyHeightCrouching);
		// }
		// return BoundingBoxUtil.within2DHeight(base,check,(headTopHeight-bodyHeight), bodyHeight);
		return false;
	}

	@Override
	public boolean intersectsBody(Location check, Entity base) {
		double height = ((Player)base).isSneaking() ? bodyHeightCrouching : bodyHeight;
		boolean intersectsBodyHeight = BoundingBoxUtil.within2DHeight(base, check, height);
		boolean intersectsBodyWidth = BoundingBoxUtil.within2DWidth(base, check, bodyWidthRadius, bodyWidthRadius);
		return intersectsBodyHeight && intersectsBodyWidth;
	}

	@Override
	public double maximumCheckingDistance(Entity base) {
		return bodyWidthRadius*2;
	}

}
