package me.zombie_striker.qg.boundingbox;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Entity;

/**
 * Bounding box in XZ plane with Y rotational freedom.
 * Intersection check is:
 * - height check: ymin < y < ymax
 * - 2D XZ plane bbox intersection check with 4 plane edges
 * 
 * 2D intersection
 * Assume box with 3 points A, B, C, AB perp. to BC
 * Consider point M, this is inside box if
 * ( 0 < AM dot AB < AB dot AB ) &&
 * ( 0 < BC dot BM < BC dot BC )
 */
public class XZBoundingBox implements AbstractBoundingBox {
	public double ymin;
	public double ymax;
	
	// points
	// a->b perpendicular b->c
	public double ax;
	public double az;
	public double bx;
	public double bz;
	public double cx;
	public double cz;

    public XZBoundingBox(double ymin, double ymax, double bx, double bz, double x, double z, double yawSin, double yawCos) {
		this.setHeight(ymin, ymax);
		this.setFromCornerAndYawSinCos(bx, bz, x, z, yawSin, yawCos);
	}

	public void setHeight(double ymin, double ymax) {
		this.ymin = ymin;
		this.ymax = ymax;
	}

	/**
	 * Set from corner position, box lengths, and pre-calculated cos, sin rotation angle
	 * @param bx corner x coord
	 * @param bz corner z coord
	 * @param x x direction distance
	 * @param z z direction distance
	 * @param yawCos cos(yaw) rotation
	 * @param yawSin sin(yaw) rotation
	 */
	public void setFromCornerAndYawSinCos(double bx, double bz, double x, double z, double yawSin, double yawCos) {
		this.bx = bx;
		this.bz = bz;
		this.ax = bx + yawCos * x;
		this.az = bz + yawSin * x;
		this.cx = bx - yawSin * z;
		this.cz = bz + yawCos * z;
	}
	
	@Override
	public boolean intersects(Entity shooter, Location check, Entity base) {
		double x = check.getX();
		double y = check.getY();
		double z = check.getZ();

		if ( y >= this.ymin && y <= this.ymax ) {
			double abx = ax - bx;
			double abz = az - bz;
			double bcx = bx - cx;
			double bcz = bz - cz;
			double amx = ax - x;
			double amz = az - z;
			double bmx = bx - x;
			double bmz = bz - z;

			double abDotAb = abx * abx + abz * abz;
			double abDotAm = abx * amx + abz * amz;
			double bcDotBc = bcx * bcx + bcz * bcz;
			double bcDotBm = bcx * bmx + bcz * bmz;

			return 0.0 <= abDotAm && abDotAm <= abDotAb && 0.0 <= bcDotBm && bcDotBm <= bcDotBc;
		}

        return false;
	}

	@Override
	public boolean allowsHeadshots() {
		return false;
	}

	@Override
	public boolean intersectsBody(Location check, Entity base) {
        return false;
	}

	@Override
	public boolean intersectsHead(Location check, Entity base) {
        return false;
	}


	@Override
	public double maximumCheckingDistance(Entity base) {
        return 1.0;
	}

	/**
	 * Perform intersect tests in a volume centered around region
	 * and visualize hit locations with particles
	 */
	public void debug(Location loc) {
		System.out.println("ymin=" + this.ymin + " ymax=" + this.ymax);
		System.out.println("ax=" + this.ax + " az=" + this.az);
		System.out.println("bx=" + this.bx + " bz=" + this.bz);
		System.out.println("cx=" + this.cx + " cz=" + this.cz);

		final double STEP = 0.5;
		
		// midpoint from A->C (middle of rect)
		final double midX = 0.5 * ax + 0.5 * cx;
		final double midZ = 0.5 * az + 0.5 * cz;


		// get extends
		final double distMidA = Math.sqrt((midX-ax)*(midX-ax) + (midZ-az)*(midZ-az));
		final double distMidB = Math.sqrt((midX-bx)*(midX-bx) + (midZ-bz)*(midZ-bz));
		final double dist = distMidA > distMidB ? 2.0 * distMidA : 2.0 * distMidB;

		System.out.println("midX=" + midX + " midZ=" + midZ + " dist=" + dist);

		World world = loc.getWorld();
		Location test = loc.clone();

		for ( double y = this.ymin; y < this.ymax; y += STEP ) {
			for ( double x = midX - dist; x < midX + dist; x += STEP ) {
				for ( double z = midZ - dist; z < midZ + dist; z += STEP ) {
					test.setX(x);
					test.setY(y);
					test.setZ(z);

					if ( this.intersects(null, test, null) ) {
						loc.getWorld().spawnParticle(
							Particle.VILLAGER_HAPPY,
							x,
							y,
							z,
							1
						);
					}
				}
			}
		}
	}
}
