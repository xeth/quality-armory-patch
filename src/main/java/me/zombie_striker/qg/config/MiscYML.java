package me.zombie_striker.qg.config;

import me.zombie_striker.qg.guns.utils.ItemType;

import java.io.File;

public class MiscYML extends ArmoryYML {

	private ItemType misctype;

	public MiscYML(File file) {
		super(file);
	}

	public void setMiscType(ItemType misctype) {
		set("MiscType",misctype.name());
		this.misctype = misctype;
	}
	public void setThrowSpeed(double speed){
		set("throwSpeed",1.5);
	}

	@Override
	public void verifyAllTagsExist() {
		super.verifyAllTagsExist();
		verify("throwSpeed",1.5);
	}
}
