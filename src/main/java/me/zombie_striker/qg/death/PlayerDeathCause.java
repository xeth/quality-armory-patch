package me.zombie_striker.qg.death;

import org.bukkit.entity.Player;

import me.zombie_striker.customitemmanager.CustomBaseObject;

public class PlayerDeathCause {
    Player killer;
    CustomBaseObject item;
    String deathMessage;
    long timestamp;

    public PlayerDeathCause(
        Player killer,
        CustomBaseObject item,
        String deathMessage,
        long timestamp
    ) {
        this.killer = killer;
        this.item = item;
        this.deathMessage = deathMessage;
        this.timestamp = timestamp;
    }

    public Player getKiller() {
        return this.killer;
    }

    public CustomBaseObject getItem() {
        return this.item;
    }

    public String getDeathMessage() {
        return this.deathMessage;
    }

    public long getTimestamp() {
        return this.timestamp;
    }
}
