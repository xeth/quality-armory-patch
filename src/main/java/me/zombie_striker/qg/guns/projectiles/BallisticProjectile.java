/**
 * Gun velocity = blocks / tick
 * Gravity = y velocity falloff / tick
 * 
 * Treat ballistic projectile as piecewise linearized hitscan:
 *    = velocity = blocks/tick
 *   <------>
 * o----------o__________
 *                       o----------o__________o
 * 
 *     t=1        t=2         t=3       t=4
 * 
 * 
 * 1. Get projected target position after dt = 1 tick = 0.05s (20 tps).
 * 2. Get direction from start to target.
 * 3. Project velocity along this direction and do linear hitscan.
 */

package me.zombie_striker.qg.guns.projectiles;

import java.util.ArrayList;
import java.util.List;

import me.zombie_striker.qg.QAMain;
import me.zombie_striker.qg.guns.Gun;
import me.zombie_striker.qg.guns.utils.GunUtil;
import me.zombie_striker.qg.guns.utils.WeaponSounds;
import me.zombie_striker.qg.handlers.ExplosionHandler;
import me.zombie_striker.qg.handlers.ParticleHandlers;

import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class BallisticProjectile implements RealtimeCalculationProjectile {
	public BallisticProjectile() {
		ProjectileManager.add(this);
	}

	@Override
	public void spawn(final Gun g, final Location s, final Player player, final Vector dir) {
        // offset start by player velocity
        s.add(player.getVelocity());
        
        new BukkitRunnable() {
            int distance = g.getMaxDistance();
            final double velocity = g.getVelocityForRealtimeCalculations();
            final Vector vel = dir.clone().multiply(velocity);
            final Vector start = new Vector(s.getX(), s.getY(), s.getZ());

            // start at 1 tick after
            int tick = 1;
            Vector previousLoc = start.clone();
            Vector currentLoc = new Vector();
            Vector direction_step = new Vector();

			@Override
			public void run() {
                // distance function for each tick step:
                // x = x0 + v0 * t + g * t^2/2
                currentLoc = currentLoc
                    .copy(vel)
                    .multiply(tick)
                    .add(start)
                    .setY(currentLoc.getY() - QAMain.gravity * tick * tick / 2);

                // linearized step
                direction_step = direction_step.copy(currentLoc).subtract(previousLoc);

                // cutoff at distance if we have reached end
                int step_distance = (int) Math.min(distance, Math.ceil(direction_step.length()));

                boolean hitSomething = GunUtil.hitscanSimple(g, player, s.clone(), direction_step, step_distance, g.getDamage());
                
                // check if past distance, 
                distance -= velocity;
                if ( hitSomething || (distance < 0) ) {
                    cancel();
                    return;
                }
                
                // update for next tick
                tick += 1;
                previousLoc = previousLoc.copy(currentLoc);
                s.add(direction_step);
			}
		}.runTaskTimer(QAMain.getInstance(), 0, 1);
	}

	@Override
	public String getName() {
		return ProjectileManager.BALLISTIC;
	}
}
