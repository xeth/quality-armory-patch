package me.zombie_striker.qg.guns.projectiles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

import me.zombie_striker.qg.QAMain;
import me.zombie_striker.qg.api.QualityArmory;
import me.zombie_striker.qg.boundingbox.AbstractBoundingBox;
import me.zombie_striker.qg.boundingbox.BoundingBoxManager;
import me.zombie_striker.qg.death.PlayerDeathCause;
import me.zombie_striker.qg.guns.Gun;
import me.zombie_striker.qg.guns.utils.GunUtil;

import me.zombie_striker.qg.handlers.WorldGuardSupport;
import net.minecraft.server.v1_16_R3.BlockBase.e;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class FireProjectile implements RealtimeCalculationProjectile {
	public FireProjectile() {
		ProjectileManager.add(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void spawn(final Gun g, final Location s, final Player player, final Vector dir) {
		Location test = player.getEyeLocation();
		double maxDist = GunUtil.getTargetedSolidMaxDistance(dir, test, g.getMaxDistance());
		
		// increase over distance to create cone effect
		double particleRandomness = 0.0;

		Vector dir2 = dir.clone().multiply(QAMain.bulletStep);

		Collection<Entity> nearby = test.getWorld().getNearbyEntities(test.clone().add(dir.clone().multiply(maxDist / 2)), maxDist / 2, maxDist / 2, maxDist / 2);

		// test.add(dir);
		for (double distance = 0; distance < maxDist; distance += QAMain.bulletStep) {
			test.add(dir2);
			particleRandomness += 0.005;

			Block bl = test.getBlock();
			Material blType = bl.getType();

			// if ( blType.name().equals("WATER") || blType.name().equals("STATIONARY_WATER") ) { // legacy
			if ( blType == Material.WATER ) {
				break;
			}

			if ( QualityArmory.allowPvPInRegion(test) ) {
				// randomly set on fire
				if ( distance > 1.0 && ThreadLocalRandom.current().nextDouble() < 0.04 ) {
					// set nearby block on fire
					if ( blType == Material.AIR ) {
						Block blBelow = bl.getRelative(0, -1, 0);
						if ( blBelow.getType().isSolid() ) {
							bl.setType(Material.FIRE);
						}
					}
					else if ( blType.isSolid() ) {
						Block blAbove = bl.getRelative(0, 1, 0);
						if ( blAbove.getType() == Material.AIR ) {
							blAbove.setType(Material.FIRE);
						}
					}
				}

				for ( Entity e : new ArrayList<>(nearby) ) {
					if (e != player && e != player.getVehicle() && e != player.getPassenger()) {
						// Double dist = test.distance(e.getLocation());
						AbstractBoundingBox box = BoundingBoxManager.getBoundingBox(e);
						if ( box.intersects(player, test, e) ) {
						// if ( dist < 1.0 ) {
							if ( e instanceof Damageable ) {
								double damage = g.getDamage();
								if ( e instanceof Player ) {
									Player target = (Player) e;
									QAMain.setPlayerDeathCause(target.getUniqueId(), new PlayerDeathCause(player, g, g.getDeathMessage(), System.currentTimeMillis()));
								}

								((Damageable) e).damage(damage, null);
								
								if (e instanceof LivingEntity) {
									((LivingEntity) e).setNoDamageTicks(0);
									try {
										if (WorldGuardSupport.canPvp(e.getLocation())) {
											e.setFireTicks(80);
										}
									} catch (Error error) {
										e.setFireTicks(80);
									}
								}
							}
							nearby.remove(e);
						}
					}
				}
			}

			try {
				if ( distance > 1.0 ) {
					test.getWorld().spawnParticle(
						Particle.FLAME,
						test,
						1,
						particleRandomness,
						particleRandomness,
						particleRandomness,
						0.01,
						null
					);
				}
			} catch (Error | Exception e) {
			}
		}
	}

	@Override
	public String getName() {
		return ProjectileManager.FIRE;
	}
}
