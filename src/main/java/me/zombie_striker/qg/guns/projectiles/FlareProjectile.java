package me.zombie_striker.qg.guns.projectiles;

import java.util.ArrayList;
import java.util.List;

import me.zombie_striker.qg.QAMain;
import me.zombie_striker.qg.api.QualityArmory;
import me.zombie_striker.qg.death.PlayerDeathCause;
import me.zombie_striker.qg.guns.Gun;
import me.zombie_striker.qg.guns.utils.GunUtil;
import me.zombie_striker.qg.guns.utils.WeaponSounds;
import me.zombie_striker.qg.handlers.ExplosionHandler;
import me.zombie_striker.qg.handlers.ParticleHandlers;

import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class FlareProjectile implements RealtimeCalculationProjectile {
	public FlareProjectile() {
		ProjectileManager.add(this);
	}

	@Override
	public String getName() {
		return ProjectileManager.FLARE;
	}

	@Override
	public void spawn(final Gun g, final Location s, final Player player, final Vector dir) {
		new BukkitRunnable() {
			int distance = g.getMaxDistance();

			@Override
			public void run() {
				dir.setY(dir.getY() - g.getGravity());
				for (int tick = 0; tick < g.getVelocityForRealtimeCalculations(); tick++) {
					distance--;
					s.add(dir);
					ParticleHandlers.spawnGunParticlesForced(g, s);
					Entity entityNearby = null;
					List<Entity> entities = new ArrayList<>(s.getWorld().getNearbyEntities(s, 0.5, 0.5, 0.5));

					for ( Entity e : entities ) {
						if (e instanceof Damageable) {
							if (QAMain.avoidTypes.contains(e.getType()))
								continue;
							
							if ( e != player && (!(e instanceof Player) || ((Player)e).getGameMode() != GameMode.SPECTATOR) ) {
								entityNearby = e;
							}
						}
					}

					boolean blockSolid = GunUtil.isSolid(s.getBlock(), s);
					if ( entityNearby != null && !blockSolid ) { // not solid, deal damage to entity and set on fire
						if ( QualityArmory.allowPvPInRegion(entityNearby.getLocation()) ) {
							// deal damage, and apply fire
							double damage = GunUtil.calculateDamage(g, entityNearby);
							if ( entityNearby instanceof Player ) {
								if ( damage >= ((Player) entityNearby).getHealth() ) {
									QAMain.setPlayerDeathCause(entityNearby.getUniqueId(), new PlayerDeathCause(player, g, g.getDeathMessage(), System.currentTimeMillis()));
								}
								((Damageable) entityNearby).damage(damage, null);

								// apply fire
								int fireticks = (int) g.getExplosionRadius();
								entityNearby.setFireTicks(fireticks);
							}
							else if ( entityNearby instanceof Damageable ) {
								((Damageable) entityNearby).damage(damage, null);

								// apply fire
								int fireticks = (int) g.getExplosionRadius();
								entityNearby.setFireTicks(fireticks);
							}
						}

						cancel();
						return;
					}
					else if ( blockSolid || distance < 0) {
						cancel();
						return;
					}
				}
			}
		}.runTaskTimer(QAMain.getInstance(), 0, 1);
	}
}
