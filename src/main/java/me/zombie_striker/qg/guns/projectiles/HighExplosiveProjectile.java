package me.zombie_striker.qg.guns.projectiles;

import java.util.ArrayList;
import java.util.List;

import me.zombie_striker.qg.QAMain;
import me.zombie_striker.qg.api.QualityArmory;
import me.zombie_striker.qg.api.QAWeaponDamageEntityEvent;
import me.zombie_striker.qg.boundingbox.AbstractBoundingBox;
import me.zombie_striker.qg.boundingbox.BoundingBoxManager;
import me.zombie_striker.qg.guns.Gun;
import me.zombie_striker.qg.guns.utils.GunUtil;
import me.zombie_striker.qg.guns.utils.WeaponSounds;
import me.zombie_striker.qg.handlers.ExplosionHandler;
import me.zombie_striker.qg.handlers.ParticleHandlers;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class HighExplosiveProjectile implements RealtimeCalculationProjectile {
	public HighExplosiveProjectile() {
		ProjectileManager.add(this);
	}

	@Override
	public void spawn(final Gun g, final Location s, final Player player, final Vector dir) {
		new BukkitRunnable() {
			int distance = g.getMaxDistance();

			@Override
			public void run() {
				dir.setY(dir.getY() - g.getGravity());
				for (int tick = 0; tick < g.getVelocityForRealtimeCalculations(); tick++) {
					distance--;
					s.add(dir);
					ParticleHandlers.spawnGunParticles(g, s);
					boolean entityNear = false;
					Entity entityNearby = null;
					List<Entity> entities = new ArrayList<>(s.getWorld().getNearbyEntities(s, 5, 5, 5));

					for ( Entity e : entities ) {
						if (e instanceof Damageable) {
							if ( QAMain.avoidTypes.contains(e.getType()) && !BoundingBoxManager.hasCustomBoundingBox(e) ) { 
								continue;
							}
		
							if ( e != player && e != player.getVehicle() ) {
								AbstractBoundingBox box = BoundingBoxManager.getBoundingBox(e);
								if ( box.intersects(player, s, e) ) {
									entityNear = true;
									entityNearby = e;
									break;
								}
							}
						}
					}

					boolean blockSolid = GunUtil.isSolid(s.getBlock(), s);
					if ( entityNear && !blockSolid ) { // not solid, set explosion location to entity
						Location entityLoc = entityNearby.getLocation();
						s.setX(entityLoc.getX());
						s.setY(entityLoc.getY());
						s.setZ(entityLoc.getZ());
					}

					if ( blockSolid || entityNear || distance < 0) {
						if ( entityNearby != null ) {
							QAWeaponDamageEntityEvent shootevent = new QAWeaponDamageEntityEvent(
								player,
								g,
								entityNearby,
								false,
								g.getDamage(),
								false
							);
							Bukkit.getPluginManager().callEvent(shootevent);
						}

						// create explosion
						World world = s.getWorld();
						if ( world != null ) {
							// pvp world guard flag to allow damage
							if ( QualityArmory.allowPvPInRegion(s) ) {
								ExplosionHandler.handleAOEExplosion(player, g, s, g.getDamage(), 1.0, g.getExplosionRadius());
							}

							// explosion world guard flag to allow block damage
							if ( QualityArmory.allowExplosionsInRegion(s) ) {
								float explosionPower = g.getBlockExplosionPower();
								if ( explosionPower > 0f ) {
									Vector explosionOffset = dir.clone().normalize();
									s.subtract(explosionOffset);
									world.createExplosion(s, explosionPower, false, true);
								}
							}

							try {
								ParticleHandlers.spawnExplosion(s);
								world.playSound(s, WeaponSounds.WARHEAD_EXPLODE.getSoundName(), 10, 1.5f);
								world.playSound(s, Sound.ENTITY_GENERIC_EXPLODE, 8, 0.7f);
							} catch (Error e3) {
								s.getWorld().playEffect(s, Effect.valueOf("CLOUD"), 0);
								try {
									world.playSound(s, Sound.valueOf("EXPLODE"), 8, 0.7f);
								} catch (Error e333) {
									world.playSound(s, Sound.valueOf("ENTITY_GENERIC_EXPLODE"), 8, 0.7f);
								}
							}
						}

						
						cancel();
						return;
					}
				}
			}
		}.runTaskTimer(QAMain.getInstance(), 0, 1);
	}

	@Override
	public String getName() {
		return ProjectileManager.HIGH_EXPLOSIVE;
	}
}
