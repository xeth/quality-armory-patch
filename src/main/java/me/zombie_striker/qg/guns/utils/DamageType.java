package me.zombie_striker.qg.guns.utils;

public enum DamageType {
    EXPLOSIVE,
    GUN,
    MACHINE_GUN,
    MISC;

    public static DamageType getByName(String name) {
		for (DamageType d : DamageType.values()) {
			if (d.name().equals(name))
				return d;
		}
		return GUN;
	}
}
