package me.zombie_striker.qg.guns.utils;

public enum ItemType {
	PISTOL(true),
	SMG(true),
	RPG(true),
	RIFLE(true),
	SHOTGUN(true),
	FLAMER(true),
	SNIPER(true),
	BIG_GUN(true),
	GRENADE(false),
	SMOKE_GRENADE(false),
	FLASHBANGS(false),
	INCENDARY_GRENADE(false),
	MOLOTOV(false),
	PROXYMINES(false),
	STICKYGRENADE(false),
	MINES(false),
	MELEE(false), 
	MISC(false),
	AMMO(false),
	HELMET(false),
	MEDKIT(false),
	LAZER(true),
	BACKPACK(false),
	PARACHUTE(false),
	CUSTOM(false);

	private boolean isGun;

	public boolean isGun() {
		return isGun;
	}

	ItemType(boolean isGun) {
		this.isGun = isGun;
	}

	public static ItemType getByName(String name) {
		for (ItemType w : ItemType.values()) {
			if (w.name().equals(name))
				return w;
		}
		return MISC;
	}
}
