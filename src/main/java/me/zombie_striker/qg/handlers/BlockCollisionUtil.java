package me.zombie_striker.qg.handlers;

import me.zombie_striker.qg.QAMain;
import me.zombie_striker.qg.guns.utils.GunUtil;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.block.data.type.Bed;
import org.bukkit.block.data.type.DaylightDetector;
import org.bukkit.block.data.type.Door;
import org.bukkit.block.data.type.Fence;
import org.bukkit.block.data.type.GlassPane;
import org.bukkit.block.data.type.Leaves;
import org.bukkit.block.data.type.Sign;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.block.data.type.TrapDoor;
import org.bukkit.block.data.type.Wall;

import java.util.EnumMap;


public class BlockCollisionUtil {

	// block collision handler interface for a specific material, format is
	// materialCollisionHandler(block, location) -> bool
	@FunctionalInterface
	private interface BlockCollisionHandler<T, U, R> {
		public R apply(T t, U u);
	}

	// private static final EnumMap<Material,Double> customBlockHeights = new EnumMap<>(Material.class);
	private static final EnumMap<Material, BlockCollisionHandler<Block, Location, Boolean>> blockCollisionHandlers = new EnumMap<>(Material.class);
	
	private static final BlockCollisionHandler<Block, Location, Boolean> defaultNoCollisionHandler = (b, l) -> false;
	private static final BlockCollisionHandler<Block, Location, Boolean> defaultSolidBlockHandler = (b, l) -> true;

	static {
		for ( Material m : Material.values() ) {
			if ( !m.isBlock() ) {
				blockCollisionHandlers.put(m, defaultNoCollisionHandler); // should never be run
				continue;
			}

			BlockData bData;
			try {
				bData = m.createBlockData();
			}
			catch (Exception e) {
				// probably not a block, this should never run
				boolean isOccluding = m.isOccluding();
				if ( isOccluding ) {
					blockCollisionHandlers.put(m, defaultSolidBlockHandler);
				}
				else {
					blockCollisionHandlers.put(m, defaultNoCollisionHandler);
				}
				continue;
			}

			if ( m == Material.SNOW ) {
				blockCollisionHandlers.put(m, defaultNoCollisionHandler);
			}

			else if ( m == Material.SCAFFOLDING ) {
				blockCollisionHandlers.put(m, defaultNoCollisionHandler);
			}

			else if( bData instanceof Sign ) {
				blockCollisionHandlers.put(m, defaultNoCollisionHandler);
			}

			else if ( m == Material.WATER) {
				blockCollisionHandlers.put(m, (b, l) -> QAMain.blockbullet_water);
			}

			else if ( bData instanceof Leaves ) {
				blockCollisionHandlers.put(m, (b, l) -> QAMain.blockbullet_leaves);
			}
			
			else if ( m == Material.ANVIL || m == Material.CHIPPED_ANVIL || m == Material.DAMAGED_ANVIL ) {
				blockCollisionHandlers.put(m, defaultSolidBlockHandler);
			}

			else if ( bData instanceof Slab ) {
				blockCollisionHandlers.put(m, (b, l) -> {
					try {
						Slab slab = (Slab) b.getBlockData();
						Slab.Type slabType = slab.getType();
						if ( slabType == Slab.Type.DOUBLE ) {
							return true;
						}
						else if ( slabType == Slab.Type.BOTTOM && (l.getY() - l.getBlockY() < 0.5 ) ) {
							return true;
						}
						else if ( slabType == Slab.Type.TOP && (l.getY() - l.getBlockY() > 0.5 ) ) {
							return true;
						}
						return false;
					}
					catch ( Exception err ) {
						System.err.println("BlockCollisionUtil.isSolid(): Slab failed");
						return false;
					}
				});
			}

			else if ( bData instanceof Bed || bData instanceof DaylightDetector ) {
				blockCollisionHandlers.put(m, (b, l) -> {
					if ( QAMain.blockbullet_halfslabs && (l.getY() - l.getBlockY() < 0.5)) {
						return true;
					}
					return false;
				});
			}

			else if ( bData instanceof Door ) {
				blockCollisionHandlers.put(m, (b, l) -> {
					try {
						Door door = (Door) b.getBlockData();
						boolean open = door.isOpen();
						Door.Hinge hinge = door.getHinge();
						BlockFace facing = door.getFacing();

						if ( open == false ) {
							switch ( facing ) {
								case NORTH:
									return (l.getZ() - l.getBlockZ() > 0.65);
								case SOUTH:
									return (l.getZ() - l.getBlockZ() < 0.35);
								case WEST:
									return (l.getX() - l.getBlockX() > 0.65);
								case EAST:
									return (l.getX() - l.getBlockX() < 0.35);
								default:
									return false;
							}
						}
						else {
							if ( hinge == Door.Hinge.RIGHT ) {
								switch ( facing ) {
									case NORTH: // open north == west
										return (l.getX() - l.getBlockX() > 0.65);
									case SOUTH: // open south == east
										return (l.getX() - l.getBlockX() < 0.35);
									case WEST: // open west == south
										return (l.getZ() - l.getBlockZ() < 0.35);
									case EAST: // open east == north
										return (l.getZ() - l.getBlockZ() > 0.65);
									default:
										return false;
								}
							}
							else {
								switch ( facing ) {
									case NORTH: // open north == east
										return (l.getX() - l.getBlockX() < 0.35);
									case SOUTH: // open south == west
										return (l.getX() - l.getBlockX() > 0.65);
									case WEST: // open west == north
										return (l.getZ() - l.getBlockZ() > 0.65);
									case EAST: // open east == south
										return (l.getZ() - l.getBlockZ() < 0.35);
									default:
										return false;
								}
							}
						}
					}
					catch ( Exception err ) {
						System.err.println("BlockCollisionUtil.isSolid(): Door failed");
						return false;
					}
				});
			}

			else if ( m.name().contains("GLASS") ) {
				blockCollisionHandlers.put(m, (b, l) -> QAMain.blockbullet_glass);
			}

			else if ( bData instanceof GlassPane ) {
				blockCollisionHandlers.put(m, (b, l) -> QAMain.blockbullet_glass);
			}

			else if ( bData instanceof TrapDoor ) {
				blockCollisionHandlers.put(m, (b, l) -> {
					try {
						TrapDoor trapdoor = (TrapDoor) b.getBlockData();
						if ( trapdoor.isOpen() ) {
							BlockFace facing = trapdoor.getFacing();
							if ( facing == BlockFace.EAST && (l.getX() - l.getBlockX()) <= 0.35 ) {
								return true;
							}
							else if ( facing == BlockFace.WEST &&  (l.getX() - l.getBlockX()) >= 0.65 ) {
								return true;
							}
							else if ( facing == BlockFace.SOUTH &&  (l.getZ() - l.getBlockZ()) <= 0.35 ) {
								return true;
							}
							else if ( facing == BlockFace.NORTH &&  (l.getZ() - l.getBlockZ()) >= 0.65 ) {
								return true;
							}
						}

						return false;
					}
					catch ( Exception err ) {
						System.err.println("BlockCollisionUtil.isSolid(): Trapdoor failed");
						return false;
					}
				});
			}

			else if ( bData instanceof Stairs ) {
				blockCollisionHandlers.put(m, (b, l) -> {
					try {
						Stairs stairs = (Stairs) b.getBlockData();
						BlockFace facing = stairs.getFacing();
						Stairs.Shape shape = stairs.getShape();
						Half vertical = stairs.getHalf();

						if ( vertical == Half.BOTTOM && (l.getY() - l.getBlockY() < 0.5)) {
							return true;
						}
						if ( vertical == Half.TOP && (l.getY() - l.getBlockY() > 0.5)) {
							return true;
						}

						// note for following below TRUE IF SOLID

						if ( facing == BlockFace.EAST) {
							switch ( shape ) {
								case STRAIGHT:
									return (l.getX() - (0.5 + l.getBlockX()) > 0);
								case INNER_LEFT: // east || north
									return (l.getZ() - (0.5 + l.getBlockZ()) > 0) || (l.getZ() - (0.5 + l.getBlockZ()) < 0);
								case INNER_RIGHT: // east || south
									return (l.getZ() - (0.5 + l.getBlockZ()) < 0) || (l.getZ() - (0.5 + l.getBlockZ()) > 0);
								case OUTER_LEFT: // east && north
									return (l.getZ() - (0.5 + l.getBlockZ()) < 0) && (l.getZ() - (0.5 + l.getBlockZ()) < 0);
								case OUTER_RIGHT: // east && south
									return (l.getZ() - (0.5 + l.getBlockZ()) > 0) && (l.getZ() - (0.5 + l.getBlockZ()) > 0);
							}
						}
						else if ( facing == BlockFace.WEST ) {
							switch ( shape ) {
								case STRAIGHT:
									return (l.getX() - (0.5 + l.getBlockX()) < 0);
								case INNER_LEFT: // west || south
									return (l.getX() - (0.5 + l.getBlockX()) < 0) || (l.getZ() - (0.5 + l.getBlockZ()) > 0);
								case INNER_RIGHT: // west || north
									return (l.getX() - (0.5 + l.getBlockX()) < 0) || (l.getZ() - (0.5 + l.getBlockZ()) < 0);
								case OUTER_LEFT: // west && south
									return (l.getX() - (0.5 + l.getBlockX()) < 0) && (l.getZ() - (0.5 + l.getBlockZ()) > 0);
								case OUTER_RIGHT: // west && north
									return (l.getX() - (0.5 + l.getBlockX()) < 0) && (l.getZ() - (0.5 + l.getBlockZ()) < 0);
							}
						}
						else if ( facing == BlockFace.NORTH ) {
							switch ( shape ) {
								case STRAIGHT:
									return (l.getZ() - (0.5 + l.getBlockZ()) < 0);
								case INNER_LEFT: // north || west
									return (l.getZ() - (0.5 + l.getBlockZ()) < 0) || (l.getX() - (0.5 + l.getBlockX()) < 0);
								case INNER_RIGHT: // north || east
									return (l.getZ() - (0.5 + l.getBlockZ()) < 0) || (l.getX() - (0.5 + l.getBlockX()) > 0);
								case OUTER_LEFT: // north && west
									return (l.getZ() - (0.5 + l.getBlockZ()) < 0) && (l.getX() - (0.5 + l.getBlockX()) < 0);
								case OUTER_RIGHT: // north && east
									return (l.getZ() - (0.5 + l.getBlockZ()) < 0) && (l.getX() - (0.5 + l.getBlockX()) > 0);
							}
						}
						else if ( facing == BlockFace.SOUTH ) {
							switch ( shape ) {
								case STRAIGHT:
									return (l.getZ() - (0.5 + l.getBlockZ()) > 0);
								case INNER_LEFT: // south || east
									return (l.getZ() - (0.5 + l.getBlockZ()) > 0) || (l.getX() - (0.5 + l.getBlockX()) > 0);
								case INNER_RIGHT: // south || west
									return (l.getZ() - (0.5 + l.getBlockZ()) > 0) || (l.getX() - (0.5 + l.getBlockX()) < 0);
								case OUTER_LEFT: // south && east
									return (l.getZ() - (0.5 + l.getBlockZ()) > 0) && (l.getX() - (0.5 + l.getBlockX()) > 0);
								case OUTER_RIGHT: // south && west
									return (l.getZ() - (0.5 + l.getBlockZ()) > 0) && (l.getX() - (0.5 + l.getBlockX()) < 0);
							}
						}

						return true;
					}
					catch ( Exception err ) {
						System.err.println("BlockCollisionUtil.isSolid(): Stairs failed");
						return false;
					}
				});
			}

			else if ( bData instanceof Wall ) {
				blockCollisionHandlers.put(m, (b, l) -> {
					try {
						Wall wallData = (Wall) b.getBlockData();
						double lx = l.getX();
						double lz = l.getZ();
						double bx = b.getX();
						double bz = b.getZ();
						
						// center of wall
						if ( lx > (bx + 0.25) && lx < (bx + 0.75) && lz > (bz + 0.25) && lz < (bz + 0.75) ) {
							return true;
						}
						if ( wallData.getHeight(BlockFace.NORTH) != Wall.Height.NONE ) {
							return lz < (bz + 0.75) && lx > (bx + 0.25) && lx < (bx + 0.75);
						}
						if ( wallData.getHeight(BlockFace.SOUTH) != Wall.Height.NONE ) {
							return lz > (bz + 0.25) && lx > (bx + 0.25) && lx < (bx + 0.75);
						}
						if ( wallData.getHeight(BlockFace.WEST) != Wall.Height.NONE ) {
							return lx < (bx + 0.75) && lz > (bz + 0.25) && lz < (bz + 0.75);
						}
						if ( wallData.getHeight(BlockFace.EAST) != Wall.Height.NONE ) {
							return lx > (bx + 0.25) && lz > (bz + 0.25) && lz < (bz + 0.75);
						}

						return false;
					}
					catch ( Exception err ) {
						System.err.println("BlockCollisionUtil.isSolid(): Wall failed");
						return false;
					}
				});
			}

			else if ( bData instanceof Fence ) {
				blockCollisionHandlers.put(m, (b, l) -> {
					double lx = l.getX();
					double lz = l.getZ();
					double bx = b.getX();
					double bz = b.getZ();
					
					// center of fence (~0.25 wide)
					if ( lx > (bx + 0.375) && lx < (bx + 0.625) && lz > (bz + 0.375) && lz < (bz + 0.625) ) {
						return true;
					}
					
					return false;
				});
			}
			
			else if ( m.name().endsWith("CARPET") ) {
				blockCollisionHandlers.put(m, defaultNoCollisionHandler);
			}

			else {
				boolean isOccluding = m.isOccluding();
				if ( isOccluding ) {
					blockCollisionHandlers.put(m, defaultSolidBlockHandler);
				}
				else {
					blockCollisionHandlers.put(m, defaultNoCollisionHandler);
				}
			}
		}
	}

	public static boolean isSolid(Block b, Location l) {
		Material m = b.getType();
		BlockCollisionHandler<Block, Location, Boolean> bHandler = BlockCollisionUtil.blockCollisionHandlers.get(m);
		return bHandler.apply(b, l);
	}

}
