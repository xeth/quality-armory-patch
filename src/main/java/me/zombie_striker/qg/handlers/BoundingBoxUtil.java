package me.zombie_striker.qg.handlers;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

public class BoundingBoxUtil {

	public static boolean within2D(Entity e, final Location closest, double minDist, double height, double centerOffset) {
		boolean b1 = within2DHeight(e, closest, height);
		boolean b2 = within2DWidth(e, closest, minDist, centerOffset);
		return b1 && b2;
	}
	public static boolean within2D(Entity e, Location closest, double minDist, double height, double heightOffset, double centerOffset) {
		boolean b1 = within2DHeight(e, closest, height, heightOffset);
		boolean b2 = within2DWidth(e, closest, minDist, centerOffset);
		return b1 && b2;
	}

	/**
	 * Note on velocity:
	 * Velocity is always (0, -0.0784000015258789, 0) except when player is falling.
	 * Velocity only changes when it needs to be applied onto player, but does not 
	 * update from user movement input.
	 */
	public static boolean within2DWidth(Entity e, Location closest, double minDist, double centerOffset) {
		// Location center = e.getLocation().clone();
		// Location centerWithVelocity = center.clone().add(e.getVelocity());
		// System.out.println("velocity=" + e.getVelocity());
		// System.out.println("center.x=" + center.getX() + " | centerWithVelocity.x=" + centerWithVelocity.getX());
		// System.out.println("center.y=" + center.getY() + " | centerWithVelocity.y=" + centerWithVelocity.getY());
		// System.out.println("center.z=" + center.getZ() + " | centerWithVelocity.z=" + centerWithVelocity.getZ());
		// double xS = (e.getLocation().clone().add(e.getVelocity()).getX()) - (closest.getX());
		// xS *= xS;
		// double zS = (e.getLocation().clone().add(e.getVelocity()).getZ()) - (closest.getZ());
		// zS *= zS;

		double xS = e.getLocation().getX() - closest.getX();
		xS *= xS;
		double zS = e.getLocation().getZ() - closest.getZ();
		zS *= zS;

		double distancesqr = xS + zS;
		return distancesqr <= (minDist*minDist);
	}

	public static boolean within2DHeight(Entity e, Location closest, double height) {
		return within2DHeight(e,closest,height,0);
	}
	public static boolean within2DHeight(Entity e, Location closest, double height, double offset) {
		// this always appear as default y velocity, need to correct for
		double VEL_Y_CORRECTION = -0.0784000015258789;
		double velY = e.getVelocity().getY() - VEL_Y_CORRECTION;
		double rel = closest.getY() - e.getLocation().getY();
		return rel >= offset && rel <= offset + height + velY;
	}
}
