package me.zombie_striker.qg.handlers;

import java.util.Map;
import org.bukkit.Location;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.zombie_striker.qg.QAMain;
import me.zombie_striker.qg.death.PlayerDeathCause;
import me.zombie_striker.qg.guns.Gun;

import org.bukkit.enchantments.Enchantment;

public class ExplosionHandler {

	// private static List<Material> indestruct = Arrays.asList(Material.OBSIDIAN,
	// Material.BEDROCK, Material.OBSERVER,
	// Material.FURNACE, Material.WATER, Material.STATIONARY_LAVA, Material.LAVA,
	// Material.STATIONARY_WATER,
	// Material.COMMAND, Material.COMMAND_CHAIN, Material.COMMAND_MINECART,
	// Material.COMMAND_REPEATING);

	public static void handleExplosion(Location origin, int radius, int power) {
		try{
			if(WorldGuardSupport.canExplode(origin)) {
				origin.getWorld().createExplosion(origin, power);
			}else{
				origin.getWorld().createExplosion(origin, 0);
			}
			if(false)
				throw new ClassNotFoundException();
		}catch(ClassNotFoundException e4){
			origin.getWorld().createExplosion(origin, power);
		}
	}
	
	/**
	 * Handle explosion following way:
	 * Two radiuses:
	 * - innerRadius: take full damage from explosion
	 * - radius: full radius of explosion (area to search for entities)
	 * 
	 * if within the inner radius, do full damage
	 * if between inner and outer radius, do falloff
	 * 
	 * does not check if innerRadius < radius
	 */
	public static void handleAOEExplosion(Player shooter, Gun gun, Location loc, double damage, double innerRadius, double radius) {
		for(Entity e : loc.getWorld().getNearbyEntities(loc, radius, radius, radius)) {
			if(e instanceof Damageable) {
				final Damageable d = (Damageable) e;
				final double dist = e.getLocation().distance(loc);
				double dmg = 0.0;
				if ( dist <= innerRadius ) {
					dmg = damage;
				}
				else {
					dmg = damage / (1.0 + dist - innerRadius);
				}
				
				if ( d instanceof Player ) {
					Player p = (Player) d;
					
					// System.out.println("entity "  + p + " dist = " + dist);

					// check for blast protection, due 20% reduction per first two levels
					for (ItemStack is : new ItemStack[]{
						p.getInventory().getHelmet(),
						p.getInventory().getChestplate(),
						p.getInventory().getLeggings(),
						p.getInventory().getBoots()}
					) {
						if ( is != null ) {
							final Map<Enchantment, Integer> enchants = is.getEnchantments();
							final Integer blastProtect = enchants.get(Enchantment.PROTECTION_EXPLOSIONS);
							if ( blastProtect != null ) {
								switch ( blastProtect ) {
									case 1:
										dmg = dmg * 0.85;
										break;
									case 2:
										dmg = dmg * 0.7;
										break;
									case 3:
										dmg = dmg * 0.55;
										break;
									case 4:
										dmg = dmg * 0.4;
										break;
									default:
										break;
								}
							}
						}
					}

					// check if dmg will kill player
					if ( dmg > p.getHealth() && shooter != null ) {
						QAMain.setPlayerDeathCause(p.getUniqueId(), new PlayerDeathCause(shooter, gun, gun.getDeathMessage(), System.currentTimeMillis()));
					}
				}

				d.damage(dmg, null);
			}
		}
	}
}
