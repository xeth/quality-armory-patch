package me.zombie_striker.qg.handlers;

import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import org.bukkit.enchantments.Enchantment;

import me.zombie_striker.qg.QAMain;

// periodic poison gas effect (apply wither + blind)
// respiration enchant protect against poison gas
public class PoisonGasHandler {
	
	static final PotionEffect poisonWither = new PotionEffect(PotionEffectType.WITHER, 61, 3);
	static final PotionEffect poisonHunger = new PotionEffect(PotionEffectType.HUNGER, 61, 3);
	static final PotionEffect poisonNausea = new PotionEffect(PotionEffectType.CONFUSION, 201, 0);
	static final PotionEffect poisonBlind = new PotionEffect(PotionEffectType.BLINDNESS, 61, 0);

	public static void spawnPoisonGas(Entity shooter, final Location loc, double damage, double radius) {
		final int RUN_PERIOD = 10;
		final int POISON_TICK_PERIOD = 4; // ticks before applying poison

		// idk why, but reduced radius looks better for particles
		final double particleRadius = radius / 1.8;

		ParticleHandlers.spawnGasCloud(loc, 140, particleRadius);
		
		new BukkitRunnable() {
			int count = 80;
			int poisonTickCounter = 0;
			World world = loc.getWorld();

			@Override
			public void run() {
				// only add more particles on early stages
				if ( count > 24 ) {
					ParticleHandlers.spawnGasCloud(loc, 12, particleRadius);
				}

				if ( poisonTickCounter <= 0 ) {
					for ( Entity e : world.getNearbyEntities(loc, radius, radius, radius) ) {
						if ( e instanceof LivingEntity ) {
							// check if armor provides poison gas protection (respiration enchant)
							if ( e.getType() == EntityType.PLAYER ) {
								Player p = (Player) e;
								ItemStack helmet = p.getInventory().getHelmet();
								if ( helmet != null ) {
									final Map<Enchantment, Integer> enchants = helmet.getEnchantments();
									if ( enchants.containsKey(Enchantment.OXYGEN ) ) {
										continue;
									}
								}

								// else, poison gas message
								try {
									HotbarMessager.sendHotBarMessage(p, ChatColor.GREEN + "Poison Gas!");
								} catch ( Exception err ) {

								}
							}

							// apply poison gas (blind + nausea + wither)
							LivingEntity ent = (LivingEntity) e;
							ent.addPotionEffect(PoisonGasHandler.poisonWither);
							ent.addPotionEffect(PoisonGasHandler.poisonHunger);
							ent.addPotionEffect(PoisonGasHandler.poisonNausea);
							ent.addPotionEffect(PoisonGasHandler.poisonBlind);
						}
					}
					poisonTickCounter = POISON_TICK_PERIOD;
				}
				else {
					poisonTickCounter -= RUN_PERIOD;
				}

				count -= 1;
				if ( count <= 0 ) {
					this.cancel();
				}
			}

		}.runTaskTimer(QAMain.getInstance(), 0, RUN_PERIOD);

		
	}
}
