package me.zombie_striker.qg.handlers;

import org.bukkit.Location;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.StateFlag;

public class WorldGuardSupport {

	public static boolean canPvp(final Location loc) {
		RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
		RegionQuery query = container.createQuery();
		ApplicableRegionSet regions = query.getApplicableRegions(BukkitAdapter.adapt(loc));
		for ( ProtectedRegion r : regions ) {
			if ( r.getFlag(Flags.PVP) == StateFlag.State.DENY ) {
				return false;
			}
		}
		
		return true;
	}

	public static boolean canExplode(final Location loc){
		RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
		RegionQuery query = container.createQuery();
		ApplicableRegionSet regions = query.getApplicableRegions(BukkitAdapter.adapt(loc));
		for ( ProtectedRegion r : regions ) {
			if ( r.getFlag(Flags.OTHER_EXPLOSION) == StateFlag.State.DENY ) {
				return false;
			}
		}

		return true;
	}
}
