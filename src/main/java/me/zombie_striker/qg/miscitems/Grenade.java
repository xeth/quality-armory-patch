package me.zombie_striker.qg.miscitems;

import java.util.List;

import me.zombie_striker.customitemmanager.CustomBaseObject;
import me.zombie_striker.customitemmanager.CustomItemManager;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import me.zombie_striker.qg.QAMain;
import me.zombie_striker.qg.api.QualityArmory;
import me.zombie_striker.qg.death.PlayerDeathCause;
import me.zombie_striker.customitemmanager.MaterialStorage;
import me.zombie_striker.qg.guns.utils.ItemType;
import me.zombie_striker.qg.guns.utils.WeaponSounds;
import me.zombie_striker.qg.handlers.ExplosionHandler;

public class Grenade extends CustomBaseObject implements ThrowableItems {

	static String deathMessage = "%player% was guro'd by %killer% using a %name%";

	double dmageLevel = 10;
	double radius = 5;
	int craftingReturn;
	
	double throwSpeed = 1.5;
	long throwRate = 2000;

	public Grenade(ItemStack[] ing, double cost, double damage, double explosionreadius, String name,
			String displayname, List<String> lore, MaterialStorage ms) {
		super(name,ms,displayname,lore,false);
		super.setIngredients(ing);
		this.setPrice(cost);
		this.radius = explosionreadius;
		this.dmageLevel = damage;
	}


	@Override
	public int getCraftingReturn() {
		return 1;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onRMB(Player thrower, ItemStack usedItem) {
		if ( QAMain.autoarm ) {
			onPull(thrower, usedItem);
		}
		if ( throwItems.containsKey(thrower) ) {
			ThrowableHolder holder = throwItems.get(thrower);
			ItemStack grenadeStack = thrower.getItemInHand();
			ItemStack temp = grenadeStack.clone();
			temp.setAmount(1);
			if (thrower.getGameMode() != GameMode.CREATIVE) {
				if (grenadeStack.getAmount() > 1) {
					grenadeStack.setAmount(grenadeStack.getAmount() - 1);
				} else {
					grenadeStack = null;
				}
				thrower.setItemInHand(grenadeStack);
			}
			Item grenade = holder.getHolder().getWorld().dropItem(holder.getHolder().getLocation().add(0, 1.5, 0),
					temp);
			grenade.setPickupDelay(Integer.MAX_VALUE);
			grenade.setVelocity(thrower.getLocation().getDirection().normalize().multiply(getThrowSpeed()));
			holder.setHolder(grenade);
			thrower.getWorld().playSound(thrower.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1.5f);

			throwItems.put(grenade, holder);
			throwItems.remove(thrower);
			QAMain.DEBUG("Throw grenade");
		} else {
			thrower.sendMessage(QAMain.prefix + QAMain.S_GRENADE_PULLPIN);
		}
		return true;
	}

	@Override
	public boolean onShift(Player shooter, ItemStack usedItem, boolean toggle) {
		return false;
	}

	@Override
	public boolean onLMB(Player e, ItemStack usedItem) {
		if(!QAMain.autoarm) {
			return onPull(e,usedItem);
		}
		return false;
	}

	public boolean onPull(Player e, ItemStack usedItem) {
		Player thrower = e.getPlayer();

		// get cooldown
		Long lastUsed = QAMain.getLastFired(thrower.getUniqueId(), ItemType.GRENADE);
		long timestamp = System.currentTimeMillis();
		if ( lastUsed != null && (timestamp - lastUsed) < this.throwRate ) {
			return true;
		}
		else {
			QAMain.setLastFired(thrower.getUniqueId(), ItemType.GRENADE, timestamp);
		}

		if( !QAMain.autoarm ) {
			if (throwItems.containsKey(thrower)) {
				thrower.sendMessage(QAMain.prefix + QAMain.S_GRENADE_PALREADYPULLPIN);
				thrower.playSound(thrower.getLocation(), WeaponSounds.RELOAD_BULLET.getSoundName(), 1, 1);
				QAMain.DEBUG("Already pin out");
				return true;
			}
		}

		thrower.getWorld().playSound(thrower.getLocation(), WeaponSounds.RELOAD_MAG_IN.getSoundName(), 2, 1);
		final ThrowableHolder h = new ThrowableHolder(thrower.getUniqueId(), thrower);
		Grenade weapon = this;
		h.setTimer(new BukkitRunnable() {
			@Override
			public void run() {
				if (h.getHolder() instanceof Player) {
					QAMain.DEBUG("Player did not throw. Damaged for " + dmageLevel);
					removeGrenade(((Player) h.getHolder()));
					((Player) h.getHolder()).damage(dmageLevel);
				}

				if (h.getHolder() instanceof Item) {
					h.getHolder().remove();
				}
				
				Location loc = h.getHolder().getLocation();
				if ( QualityArmory.allowPvPInRegion(loc) ) {
					if ( QAMain.enableExplosionDamage ) {
						ExplosionHandler.handleExplosion(loc, 3, 1);
						QAMain.DEBUG("Using default explosions");
					}

					try {
						h.getHolder().getWorld().spawnParticle(org.bukkit.Particle.EXPLOSION_HUGE, loc, 0);
						h.getHolder().getWorld().playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 8, 0.7f);
					} catch (Error e3) {
						h.getHolder().getWorld().playEffect(loc, Effect.valueOf("CLOUD"), 0);
						h.getHolder().getWorld().playSound(loc, Sound.valueOf("EXPLODE"), 8, 0.7f);
					}
					Player thro = Bukkit.getPlayer(h.getOwner());
					try {
						for (Entity e : h.getHolder().getNearbyEntities(radius, radius, radius)) {
							if (e instanceof LivingEntity) {
								double dam = (dmageLevel / (e.getLocation().distance(loc) + 0.00001) );
								QAMain.DEBUG("Grenade-Damaging " + e.getName() + " : " + dam + " DAM.");

								// player death message
								if ( e instanceof Player ) {
									if ( dam >= ((Player) e).getHealth() ) {
										QAMain.setPlayerDeathCause(e.getUniqueId(), new PlayerDeathCause(thro, weapon, Grenade.deathMessage, System.currentTimeMillis()));
									}
								}

								((LivingEntity) e).damage(dam, null);
							}
						}
					} catch (Error e) {
						// h.getHolder().getWorld().createExplosion(loc, 1);
						// QAMain.DEBUG("Failed. Created default explosion");
					}
				}
				
				throwItems.remove(h.getHolder());
			}
		}.runTaskLater(QAMain.getInstance(), 5 * 20));
		throwItems.put(thrower, h);
		return true;
	}

	@Override
	public boolean is18Support() {
		return false;
	}

	@Override
	public void set18Supported(boolean b) {

	}

	@Override
	public ItemStack getItemStack() {
		return CustomItemManager.getItemType("gun").getItem(this.getItemData().getMat(),this.getItemData().getData(),this.getItemData().getVariant());
	}

	public void removeGrenade(Player player) {
		if (player.getGameMode() != GameMode.CREATIVE) {
			int slot = -56;
			ItemStack stack = null;
			for (int i = 0; i < player.getInventory().getContents().length; i++) {
				if ((stack = player.getInventory().getItem(i)) != null && MaterialStorage.getMS(stack) == getItemData()) {
					slot = i;
					break;
				}
			}
			if (slot >= -1) {
				if (stack.getAmount() > 1) {
					stack.setAmount(stack.getAmount() - 1);
				} else {
					stack = null;
				}
				player.getInventory().setItem(slot, stack);
			}
		}
	}


	@Override
	public boolean onSwapTo(Player shooter, ItemStack usedItem) {
		if (getSoundOnEquip() != null)
			shooter.getWorld().playSound(shooter.getLocation(), getSoundOnEquip(), 1, 1);
		return false;
	}

	@Override
	public boolean onSwapAway(Player shooter, ItemStack usedItem) {
		return false;
	}

	@Override
	public double getThrowSpeed() {
		return throwSpeed;
	}

	@Override
	public void setThrowSpeed(double t) {
		throwSpeed = t;
	}

	@Override
	public long getThrowRate() {
		return throwRate;
	}

	@Override
	public void setThrowRate(double t) {
		this.throwRate = (long) t * 1000;
	}
}
